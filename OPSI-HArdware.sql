select
		HARDWARE_CONFIG_NETWORK_CONTROLLER.hardware_id,
      name,
      hostId,
      macaddress
from
      HARDWARE_DEVICE_NETWORK_CONTROLLER,
      HARDWARE_CONFIG_NETWORK_CONTROLLER
where
		HARDWARE_CONFIG_NETWORK_CONTROLLER.hardware_id = HARDWARE_DEVICE_NETWORK_CONTROLLER.hardware_id
	AND 
		macaddress IS NOT NULL
	AND
		macaddress <> ''
   AND
      NAME LIKE '%Wireless%'
      
order by hostId;